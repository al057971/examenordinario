/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arbol;

/**
 *
 * @author Cristian Berzunza Poot
 */
public class Arbol {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int dato;
        String nombre;
        ArbolBinario miArbol = new ArbolBinario();
        miArbol.agregarNodo(79, "c");
        miArbol.agregarNodo(82, "R");
        miArbol.agregarNodo(76, "I");
        miArbol.agregarNodo(65, "A");
        miArbol.agregarNodo(78, "N");
        miArbol.agregarNodo(68, "B");
        miArbol.agregarNodo(73, "E");
        miArbol.agregarNodo(69, "Z");
        miArbol.agregarNodo(67, "P");
        miArbol.agregarNodo(72, "O");
        miArbol.agregarNodo(83, "T");
        
        
        System.out.println("PreOrden");
        if (!miArbol.estaVacio()){
            miArbol.preOrden(miArbol.raiz);
        }
        
        
    }
    
}
